﻿using System;

namespace Mesozoic
{
    public abstract class Dinosaur
    {
        protected string name;
        protected virtual string specie { get { return "Dinosaur"; } }
        protected int age;


        public Dinosaur(string name, int age)
        {
            this.name = name;
            this.age = age;
        }

        public virtual string sayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, this.specie, this.age);
        }

        public virtual string roar()
        {
            return "Grrr";
        }
        public string getName()
        {
            return this.name;
        }
        public string getSpecie()
        {
            return this.specie;
        }
        public int getAge()
        {
            return this.age;
        }
        public void setName(string name)
        {
            this.name = name;
        }
        public void setAge(int age)
        {
            this.age = age;
        }
        public string hug(Dinosaur otherdino)
        {
            return string.Format("Je suis {0} et je fais un calin à {1}.", this.name, otherdino.getName());
        }
        public override string ToString()
        {
            return String.Format("name : {0}, age : {1}, specie : {2}", this.name, this.age, this.specie);
        }
        public override int GetHashCode()
        {
            int hash = 13;
            hash ^= this.name.GetHashCode();
            hash ^= this.age.GetHashCode();
            hash ^= this.specie.GetHashCode();
            return hash;
        }
        public static bool operator ==(Dinosaur dino1, Dinosaur dino2)
        {
            return dino1.GetHashCode() == dino2.GetHashCode();
        }

        public static bool operator !=(Dinosaur dino1, Dinosaur dino2)
        {
            return !(dino1 == dino2);
        }
        public override bool Equals(object obj)
        {
            return obj is Dinosaur && this == (Dinosaur)obj;

        }   
    }
}