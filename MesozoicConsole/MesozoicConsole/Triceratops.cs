﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;

namespace Trice
{
    public class Triceratops : Dinosaur
    {
        protected override string specie { get { return "Triceratops"; } }
        public Triceratops(string name, int age) : base(name, age)
        {

        }
        public override string roar()
        {
            return "Ummmmrrrr";
        }
        public override string sayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans. J'empale les dinos.", this.name, this.specie, this.age);
        }
    }
}
