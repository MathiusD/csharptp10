﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;

namespace Diplo
{
    public class Diplodocus : Dinosaur
    {
        protected override string specie { get { return "Diplodocus"; } }
        public Diplodocus(string name, int age) : base(name, age)
        {

        }
        public override string roar()
        {
            return "HmmMMMMmmmmhmhmhmhmhmmmmhmmm";
        }
        public override string sayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans. Je mange des feuilles d'arbres.", this.name, this.specie, this.age);
        }
    }
}
