﻿using System;
using Mesozoic;
using Diplo;
using Stego;
using TRex;
using Trice;
using Dino;

namespace Ingen
{
    public class Laboratory
    {
        public static TDinosaur CreateDinosaur<TDinosaur>(string name) where TDinosaur : Dinosaur
        {
            return (TDinosaur)Activator.CreateInstance(typeof(TDinosaur), name, 0);
        }
    }
}